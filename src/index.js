import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
// You can choose your kind of history here (e.g. browserHistory)
import ReactDOM from 'react-dom'
// Your routes.js file
/**
 * Import all page components here
 */
import MyRewards from './pages/users/me'
import Loyalty from './pages/users/loyalty'
import AllRewards from './pages/users/all'
import ResetPassword from './pages/users/reset'
import Login from './pages/login'
import Home from './pages/home.js'
import Workout from './pages/users/workout'
// import MainAdmin from './pages/admin/';
import CreateChallenge from './pages/admin/challenges/create'
import VerifyAccount from './pages/users/vetifyAccount'
import Challenges from './pages/admin/challenges'
import EditChallenge from './pages/admin/challenges/edit'

/**
 * All routes go here.
 * Don't forget to import the components above after adding new route.
 */
const token = localStorage.getItem('token')
const types = localStorage.getItem('types')
console.log('index root ', token)
console.log('index root ', types)

const route = (
  <Router>
    <div>
      <Route path='/login' component={Login} />
      <Route exact path='/' component={Home} />
      <Route path='/reward/me' component={MyRewards} />
      <Route path='/reward/all' component={AllRewards} />
      <Route path='/loyalty-program' component={Loyalty} />
      <Route path='/reset-password' component={ResetPassword} />
      <Route path='/verify' component={VerifyAccount} />
      <Route path='/workout/:id' component={Workout} />

      {/* Page Admin */}
      {/*<Route path='/admin' component={MainAdmin} />*/}
      <Route path='/admin/challenges/create' component={CreateChallenge} />
      <Route exact path='/admin/challenges' component={Challenges} />
      <Route path='/admin/challenges/edit/:id' component={EditChallenge} />
    </div>
  </Router>
)

ReactDOM.render(route, document.getElementById('root'))
