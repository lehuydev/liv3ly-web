export const wp = window.innerWidth
export const hp = window.innerHeight

export const Backgrounds = {
  Bg1: '../images/imgSgd5.png',
  Bg2: '../images/imgSgd10.png',
  Bg3: '../images/imgRaceDay.png',
  BgLogin: '../images/loginbg.png',
  BgChallenges: '../images/imgChallengebg.png',
  Logo: '../images/logo.svg',
  BgWorkout: '../images/imgWinBg.svg'
}

export const Icons = {
  icNotDone: '../images/icNotDone.svg',
  icDone: '../images/icDone.svg',
  icError: '../images/icError.svg',
  icGift: '../images/icGift.svg',
  icCheck: '../images/icCheck.svg',
  icAdd: '../images/icAdd.svg',
  icDel: '../images/icDelete.svg',
  icView: '../images/icView.svg',
  icEdit: '../images/icEdit.svg',
  icPace: '../images/icPace.svg',
  icDuration: '../images/icDuration.svg'
}

export const Params = {
  API_URL: 'http://api.prod.liv3ly.io/api/v1'
}

export const Regex = {
  emailRegex: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  passwordRegex: /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/,
  passwordCharacters: 8,
  phoneRegex: /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/,
  nameCharacters: 4,
  phoneNumberCharacters: 8
}

export const Colors = {
  white: '#ffffff',
  blueTable: '#067af7',
  grayTable: '#f4f4f4'
}

export const METHOD_GET = 'GET'
export const METHOD_POST = 'POST'
export const METHOD_DEL = 'DELETE'
export const DATE_FORMAT = 'DD/MM/YYYY HH:mm'
