import styled from 'styled-components'

export const UploadStyle = styled.div`
  .avatar-uploader > .ant-upload {
    width: 100px;
    height: 100px;
  }
`
