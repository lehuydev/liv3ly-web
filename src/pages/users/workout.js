import React, { Component } from 'react'
import { Helmet } from 'react-helmet'
import { Params, METHOD_GET } from '../../contants/contants'
import { Icons, Backgrounds } from '../../contants/contants'
import { convertTime } from '../../helper'

class Workout extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      workoutDetail: []
    }
  }

  componentDidMount() {
    let session_id = this.props.match.params.id
    this.getWorkoutDetail(session_id)
  }

  getWorkoutDetail = async workout_id => {
    const url = Params.API_URL + `/account/session/${workout_id}/workout-detail`
    const data = await fetch(url, {
      method: METHOD_GET,
      headers: {
        'Content-Type': 'application/json'
      }
    })
      .then(response => response.json())
      .catch(e => e)

    if (data.status_code === 1000) {
      this.setState({ workoutDetail: data.data })
    }
  }

  render() {
    const {
      name,
      duration,
      distance,
      pace,
      earning_amount
    } = this.state.workoutDetail

    return (
      <div
        className='text-center'
        style={{
          backgroundColor: '#2B2B2B',
          color: '#fff',
          padding: '72px 16px 72px',
          minHeight: '100vh'
        }}
      >
        {/* <Helmet title='Review'>
          <title>Review</title>
          <meta name='title' content='Review' />
          <meta name='description' content='Workout review' />
          <meta property='og:title' content='Review' />
          <meta property='og:description' content='Workout description' />
          <meta property='og:url' content='https://' />
          <meta property='og:site_name' content='Review' />
          <meta property='article:section' content='Article' />
        </Helmet> */}

        {/**/}
        <div
          className=' d-flex flex-column justify-content-center align-items-center mb-4'
          style={{
            textAlign: 'center',
            margin: '0 auto',
            width: '240px',
            height: '240px',
            backgroundImage: `url(${Backgrounds.BgWorkout})`,
            backgroundPosition: 'center center',
            backgroundSize: 'cover'
          }}
        >
          <p className='mb-0'>
            <span
              style={{
                fontSize: '42px',
                lineHeight: '42px',
                fontWeight: 'bold'
              }}
            >
              {Math.round(distance * 100) / 100}
            </span>{' '}
            {/* / <span className='font-weight-bold'>5.0</span> */}
          </p>
          <span style={{ color: 'rgba(255, 255, 255, 0.8)' }}>KILOMETERS</span>
        </div>
        <p style={{ color: 'rgba(255,255,255,0.6)' }}>You have completed</p>
        <h2
          style={{
            fontSize: '36px',
            color: '#fff',
            fontWeight: 'bold',
            textTransform: 'uppercase'
          }}
        >
          {name}
        </h2>
        <p>
          <span
            className='font-weight-bold'
            style={{ color: 'rgb(255,160, 0)' }}
          >
            +{earning_amount} bonus Points
          </span>{' '}
          <br />
          <span style={{ color: 'rgba(255, 255, 255, 0.6)' }}>
            for this achievement.
          </span>
        </p>
        <hr
          className='mt-4 mb-4'
          style={{ backgroundColor: 'rgba(255, 255, 255, 0.1)' }}
        />
        <div className='container d-flex justify-content-center'>
          <div className='row mr-5'>
            <div className='mr-2'>
              <img width='20' height='20' src={Icons.icPace} />
            </div>
            <div className='text-left'>
              <span className='font-weight-bold'>
                <span style={{ fontSize: '24px', lineHeight: '24px' }}>
                  {Math.round(pace * 100) / 100}
                </span>{' '}
                min/km
              </span>
              <br />
              <span style={{ color: 'rgba(255,255,255,0.6)' }}>Pace</span>
            </div>
          </div>
          <div className='row'>
            <div className='mr-2'>
              <img width='20' height='20' src={Icons.icDuration} />
            </div>
            <div className='text-left'>
              <span className='font-weight-bold'>
                <span style={{ fontSize: '24px', lineHeight: '24px' }}>
                  {convertTime(duration)}
                </span>{' '}
                min
              </span>
              <br />
              <span style={{ color: 'rgba(255,255,255,0.6)' }}>Duration</span>
            </div>
          </div>
        </div>
        {/* <div className='row'>
          <img className='mw-100 mx-auto' src='../images/session_image.png' />
        </div> */}
      </div>
    )
  }
}

export default Workout
