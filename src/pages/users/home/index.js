import React from 'react'
import { Button } from 'antd'
import { Link, Redirect } from 'react-router-dom'
import jwt from 'jsonwebtoken'

class HomeUser extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      redirectToReferrer: false
    }
  }

  render() {
    // const {redirectToReferrer} = this.state
    // let { from } = this.props.location.state || { from: { pathname: "/login" } };

    // if (redirectToReferrer) return <Redirect to={from} />;
    let token = localStorage.getItem('token')
    var user = jwt.decode(token)
    if (user) {
      return <Redirect to={'reward/all'} />
    } else {
      return <Redirect to={'/login'} />
    }
  }
}

export default HomeUser
