import React from 'react'
import { Link, Redirect } from 'react-router-dom'
import { Form, Icon, Input, Button, Spin, Modal } from 'antd'
import HomeAdmin from './admin/homeadmin/index'
import HomeUser from './users/home'
import jwt from 'jsonwebtoken'
class Home extends React.Component {
  constructor() {
    super()
    this.state = {
      redirectToReferrer: false,
      isAdmin: false
    }
  }
  componentWillMount() {
    let token = localStorage.getItem('token')
    let types = localStorage.getItem('types')
    const user = jwt.decode(token)
    if (user && types) {
      console.log(types)
      if (types === '1') {
        this.setState({
          isAdmin: true
        })
      }
    } else {
      this.setState({ redirectToReferrer: true })
    }
  }
  componentDidMount() {}

  logout = () => {
    localStorage.clear()
    this.setState({ redirectToReferrer: true })
  }

  render() {
    const { redirectToReferrer, isAdmin } = this.state
    let { from } = this.props.location.state || { from: { pathname: '/login' } }

    if (redirectToReferrer) return <Redirect to={from} />
    console.log(isAdmin)

    return (
      <div className='site_wrapper pl-3 pr-3' id='site_wrapper'>
        <Button onClick={this.logout}>Logout</Button>
        {isAdmin ? <HomeAdmin /> : <HomeUser />}
      </div>
    )
  }
}

export default Home
