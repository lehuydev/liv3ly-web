import React from 'react'
import { Button } from 'antd'
import { Link, Redirect } from 'react-router-dom'
import jwt from 'jsonwebtoken'

class HomeAdmin extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      redirectToReferrer: false
    }
  }

  componentWillMount() {}

  componentDidMount() {
    // let checkTypes = localStorage.getItem("types")
    // var isAdmin = checkTypes.includes(1);
    // let checkLogin = localStorage.getItem("token")
    // if(checkLogin.length === 0){
    //   this.setState({redirectToReferrer: true})
    // }
  }

  logout = () => {
    console.log('logout')
    localStorage.clear()
  }

  render() {
    let token = localStorage.getItem('token')
    var user = jwt.decode(token)
    if (user) {
      console.log(user)
      return <Redirect to={'/admin/challenges'} />
    } else {
      return <Redirect to={'/login'} />
    }
    // const {redirectToReferrer} = this.state
    // let { from } = this.props.location.state || { from: { pathname: "/login" } };
    // if (redirectToReferrer) return <Redirect to={from} />;
  }
}

export default HomeAdmin
