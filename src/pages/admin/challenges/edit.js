import React from 'react'
import { Backgrounds } from '../../../contants/contants'
import { Button } from 'antd'
import FormChallenge from './form'

import { Redirect } from 'react-router-dom'
import { Container, Component, ScrollView } from './style/edit.style'
import 'antd/dist/antd.css'

class EditChallenge extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      redirectToReferrer: false
    }
  }

  render() {
    const { redirectToReferrer } = this.state
    let { from } = this.props.location.state || {
      from: { pathname: '/admin/challenges' }
    }

    if (redirectToReferrer) return <Redirect to={from} />
    let id = this.props.match.params.id
    console.log(id)

    return (
      <Container
        // style={{
        //   backgroundImage: `url(${Backgrounds.BgChallenges})`,
        //   backgroundPosition: 'center center',
        //   backgroundSize: 'cover'
        // }}
        className='site_wrapper d-flex flex-column align-items-center justify-content-center'
        id='site_wrapper'
      >
        <Component>
          <FormChallenge id={id} type='edit' />
        </Component>
      </Container>
    )
  }
}

export default EditChallenge
