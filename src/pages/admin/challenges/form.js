import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Form, Collapse, Button, Alert, Image } from 'react-bootstrap'
import { DatePicker, Icon, Spin } from 'antd'
import moment from 'moment'
import { Params, DATE_FORMAT, METHOD_GET } from '../../../contants/contants'
import ImageUpload from '../../../components/upload'
import { getBase64, urlToBase64 } from '../../../helper'
import Header from 'antd/lib/calendar/Header'
import Notification from '../../../components/notification'

const { RangePicker } = DatePicker
const infinityTime = Math.round(
  moment('99991231', 'YYYYMMDD')._d.getTime() / 1000
)
class FormChallenge extends Component {
  constructor(props) {
    super(props)
    this.state = {
      challengeDetail: {
        title: '',
        photo: '',
        venue: '',
        description: '',
        start_time: 0,
        end_time: 0,
        is_private: 0,
        pwdToJoin: '',
        slots: 0,
        is_consecutive_days: 0,
        criteria: [
          {
            reward: 0,
            participation_fee: 0,
            conditions: [
              {
                type: 1,
                value: '0',
                unit: 'm'
              }
            ]
          }
        ]
      },
      validated: false,
      type: 'create',
      uploading: false,
      no_end_date: false,
      isLoading: false
    }
  }

  componentDidMount() {
    this.setState({ type: this.props.type })
    if (this.props.type == 'edit') {
      let id = this.props.id
      this.getChallengeDetail(id)
    }
  }

  getChallengeDetail = async id => {
    let url = Params.API_URL + `/admin/challenge/detail/${id}`
    let token = localStorage.getItem('token')
    // console.log(url)
    const data = await fetch(url, {
      method: METHOD_GET,
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token
      }
    })
      .then(response => response.json())
      .catch(e => e)

    console.log('data', data)
    if (data.status_code == 1000) {
      const {
        id,
        title,
        photo,
        venue,
        description,
        start_time,
        end_time,
        pwdToJoin,
        slots,
        isConsecutiveDays,
        criteria,
        is_private
      } = data.data

      if (end_time === infinityTime) {
        this.setState({ no_end_date: true })
      }

      this.setState({
        challengeDetail: {
          ...this.state.challengeDetail,
          id,
          title,
          venue,
          photo,
          description,
          start_time,
          end_time,
          pwdToJoin,
          is_private,
          slots,
          criteria,
          is_consecutive_days: isConsecutiveDays
        }
      })
    } else {
      this.setState({ notiText: data.message })
    }
  }

  uploadImage = async file => {
    const base64 = await getBase64(file)
    if (base64) {
      console.log(base64)
      this.setState({
        challengeDetail: { ...this.state.challengeDetail, photo: base64 }
      })
    }
  }

  renderUnitSelect = (units, onChange, value) => {
    return (
      <Form.Control
        className='mb-1'
        onChange={onChange}
        value={value}
        as='select'
      >
        {units.map(unit => {
          return <option value={unit}>{unit}</option>
        })}
      </Form.Control>
    )
  }

  renderCondition = conditions => {
    const { challengeDetail } = this.state
    let units = ['days', 'weeks', 'months', 'hours', 'minutes']

    let options = [
      {
        value: 1,
        label: 'Distance'
      },
      {
        value: 2,
        label: 'Duration'
      }
    ]

    return conditions.map((condition, i) => {
      if (challengeDetail.is_consecutive_days === 0) {
        switch (challengeDetail.criteria[0].conditions[i].type) {
          case 1:
            units = ['m', 'km']
            break
          case 2:
            units = ['days', 'weeks', 'months', 'hours', 'minutes']
            break
          // case 'time':
          //   units = ['hour', 'minute']
          //   break
          default:
            break
        }
      } else {
        units = ['days']
        options = [
          {
            value: 2,
            label: 'Duration'
          }
        ]
      }
      console.log('render condition')

      return (
        <Form.Group>
          <Form.Control
            className='mb-1'
            onChange={e => {
              const currentCriteria = challengeDetail.criteria
              const currentConditions = currentCriteria[0].conditions
              currentConditions[i].type = Number(e.currentTarget.value)
              let unit
              switch (e.currentTarget.value) {
                case '1':
                  unit = 'm'
                  break
                case '2':
                  unit = 'days'
                  break
                // case 'time':
                //   unit = 'hour'
                //   break
                default:
                  break
              }
              currentConditions[i].unit = unit
              currentCriteria[0].conditions = currentConditions
              this.setState({
                challengeDetail: {
                  ...challengeDetail,
                  criteria: currentCriteria
                }
              })
            }}
            defaultValue='1'
            value={condition.type}
            as='select'
          >
            {options.map(option => {
              return <option value={option.value}>{option.label}</option>
            })}
            {/* <option value='time'>Time</option> */}
          </Form.Control>
          {this.renderUnitSelect(
            units,
            e => {
              const currentCriteria = challengeDetail.criteria
              const currentConditions = currentCriteria[0].conditions
              currentConditions[i].unit = e.currentTarget.value
              currentCriteria[0].conditions = currentConditions
              this.setState({
                challengeDetail: {
                  ...challengeDetail,
                  criteria: currentCriteria
                }
              })
            },
            challengeDetail.criteria[0].conditions[i].unit
          )}
          <Form.Control
            required
            type='text'
            placeholder='Value'
            value={challengeDetail.criteria[0].conditions[i].value}
            onChange={e => {
              if (!isNaN(e.target.value)) {
                const currentCriteria = challengeDetail.criteria
                const currentConditions = currentCriteria[0].conditions
                currentConditions[i].value = e.target.value
                currentCriteria[0].conditions = currentConditions
                this.setState({
                  challengeDetail: {
                    ...challengeDetail,
                    criteria: currentCriteria
                  }
                })
              }
            }}
          />
        </Form.Group>
      )
    })
  }

  renderCriteria = () => {
    const { challengeDetail } = this.state
    if (!challengeDetail.criteria) {
      return
    }

    return (
      <div>
        <Form.Group>
          <Form.Label>Reward</Form.Label>
          <Form.Control
            required
            type='text'
            placeholder='Reward'
            value={challengeDetail.criteria[0].reward}
            defaultValue={
              challengeDetail.criteria[0].reward
                ? challengeDetail.criteria[0].reward
                : 0
            }
            onChange={e => {
              if (!isNaN(e.target.value)) {
                const currentCriteria = challengeDetail.criteria
                currentCriteria[0].reward = Number(e.target.value)
                this.setState({
                  challengeDetail: {
                    ...challengeDetail,
                    criteria: currentCriteria
                  }
                })
              }
            }}
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Participation fee</Form.Label>
          <Form.Control
            required
            type='text'
            placeholder='Participation fee'
            value={challengeDetail.criteria[0].participation_fee}
            defaultValue={
              challengeDetail.criteria[0].participation_fee
                ? challengeDetail.criteria[0].participation_fee
                : 0
            }
            onChange={e => {
              if (!isNaN(e.target.value)) {
                const currentCriteria = challengeDetail.criteria
                currentCriteria[0].participation_fee = Number(e.target.value)
                this.setState({
                  challengeDetail: {
                    ...challengeDetail,
                    criteria: currentCriteria
                  }
                })
              }
            }}
          />
        </Form.Group>
        {this.state.type === 'create' ? (
          <Form.Group>
            <Form.Check
              checked={challengeDetail.is_consecutive_days === 1 ? true : false}
              onChange={e => {
                const currentCriteria = challengeDetail.criteria
                const currentConditions = currentCriteria[0].conditions
                if (e.target.checked) {
                  currentConditions.map(cdt => {
                    cdt.type = 2
                    cdt.unit = 'days'
                  })
                  currentCriteria[0].conditions = currentConditions
                }
                this.setState({
                  challengeDetail: {
                    ...challengeDetail,
                    criteria: currentCriteria,
                    is_consecutive_days: e.target.checked ? 1 : 0
                  }
                })
              }}
              label='Consecutive days'
            />
          </Form.Group>
        ) : challengeDetail.is_consecutive_days === 1 ? (
          <div className='d-flex align-items-center mb-3'>
            <Icon className='mr-2' type='check' /> Consecutive day
          </div>
        ) : (
          <div className='d-flex align-items-center mb-3'>
            <Icon className='mr-2' style={{ color: 'red' }} type='close' />{' '}
            Consecutive day
          </div>
        )}
        <Form.Label>Conditions</Form.Label>
        {this.renderCondition(challengeDetail.criteria[0].conditions)}
      </div>
    )
  }

  addCondition = () => {
    const currentCriteria = this.state.challengeDetail.criteria
    const currentConditions = currentCriteria[0].conditions
    const newCondition = {
      type: 1,
      value: '0',
      unit: 'm'
    }
    currentConditions.push(newCondition)
    currentCriteria[0].conditions = currentConditions
    this.setState({
      challengeDetail: {
        ...this.state.challengeDetail,
        criteria: currentCriteria
      }
    })
  }

  createChallenge = params => {
    console.log(params)
    this.setState({ isLoading: true })

    const url = Params.API_URL + '/admin/challenge/add'
    let token = localStorage.getItem('token')
    let headers = new Headers()
    headers.append('Content-Type', 'application/json')
    headers.append('Authorization', `Bearer ${token}`)
    fetch(url, {
      method: 'POST',
      headers: headers,
      body: JSON.stringify(params)
    })
      .then(response => response.json())
      .then(data => {
        if (data.status_code === 1000) {
          Notification('success', 'SUCCESS', 'Create challenge successfully!')
          setTimeout(() => {
            window.location.href = '/admin/challenges'
          }, 2000)
          this.setState({ isLoading: false })
        } else {
          Notification(
            'error',
            'FAILURE',
            'Fail to create challenge. Please try again!'
          )
        }
      })
      .catch(error => error)
  }

  updateChallenge = params => {
    this.setState({ isLoading: true })
    const url = Params.API_URL + '/admin/challenge/edit'
    let token = localStorage.getItem('token')
    const headers = new Headers()
    headers.append('Content-Type', 'application/json')
    headers.append('Authorization', `Bearer ${token}`)
    console.log(JSON.stringify(params))
    fetch(url, {
      method: 'PUT',
      headers: headers,
      body: JSON.stringify(params)
    })
      .then(response => response.json())
      .then(data => {
        if (data.status_code === 1000) {
          Notification('success', 'SUCCESS', 'Updated challenge successfully!')
          this.setState({ isLoading: false })
          setTimeout(() => {
            window.location.href = '/admin/challenges'
          }, 2000)
        } else {
          Notification(
            'error',
            'FAILURE',
            'Fail to update challenge. Please try again!'
          )
        }
      })
      .catch(error => error)
  }

  onFormSubmit = e => {
    const form = e.currentTarget
    e.preventDefault()
    e.stopPropagation()
    if (form.checkValidity() === true) {
      this.setState({ validated: true })
      if (this.state.type == 'edit') {
        if (this.state.challengeDetail.photo.indexOf('http') === 0) {
          this.setState(
            {
              challengeDetail: {
                ...this.state.challengeDetail,
                photo: ''
              }
            },
            () => {
              console.log(this.state.challengeDetail)
              this.updateChallenge(this.state.challengeDetail)
            }
          )
        } else {
          this.updateChallenge(this.state.challengeDetail)
        }
        // call api edit
      } else {
        this.createChallenge(this.state.challengeDetail)
        // call api create
      }
    } else {
      Notification('error', 'FAILURE', 'Invalid form. Please try again!')
    }
  }

  render() {
    const {
      title,
      venue,
      description,
      photo,
      start_time,
      end_time,
      is_private,
      pwdToJoin,
      slots,
      is_consecutive_days,
      criteria
    } = this.state.challengeDetail
    const { challengeDetail, isLoading } = this.state

    return (
      <Spin spinning={isLoading} delay={500} tip='Loading...'>
        <Form
          noValidate
          validated={this.state.validated}
          onSubmit={e => this.onFormSubmit(e)}
        >
          <Form.Group>
            <Form.Label>Title</Form.Label>
            <Form.Control
              required
              type='text'
              placeholder='Title'
              value={title}
              onChange={e => {
                this.setState({
                  challengeDetail: { ...challengeDetail, title: e.target.value }
                })
              }}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Venue</Form.Label>
            <Form.Control
              required
              type='text'
              placeholder='Venue'
              value={venue}
              onChange={e => {
                this.setState({
                  challengeDetail: { ...challengeDetail, venue: e.target.value }
                })
              }}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Description</Form.Label>
            <Form.Control
              required
              type='text'
              placeholder='Description'
              value={description}
              onChange={e => {
                this.setState({
                  challengeDetail: {
                    ...challengeDetail,
                    description: e.target.value
                  }
                })
              }}
            />
          </Form.Group>
          <Form.Group>
            <Form.Label>Slots</Form.Label>
            <Form.Control
              required
              type='text'
              value={slots}
              onChange={e => {
                if (!isNaN(e.target.value)) {
                  this.setState({
                    challengeDetail: {
                      ...challengeDetail,
                      slots: Number(e.target.value)
                    }
                  })
                }
              }}
            />
          </Form.Group>
          <Form.Label>Start date</Form.Label>
          <br />
          <DatePicker
            placeholder={
              this.state.type === 'edit' && start_time != 0
                ? moment(start_time * 1000).format(DATE_FORMAT)
                : 'start date'
            }
            showTime
            onChange={date => {
              let start_time = Math.round(date._d.getTime() / 1000)
              this.setState({
                challengeDetail: { ...challengeDetail, start_time }
              })
            }}
          />
          <br />
          <Form.Label className={this.state.no_end_date ? 'd-none' : ''}>
            End date
          </Form.Label>
          <br />
          <DatePicker
            className={this.state.no_end_date ? 'd-none' : ''}
            showTime
            placeholder={
              this.state.type === 'edit'
                ? moment(end_time * 1000).format(DATE_FORMAT)
                : 'end_date'
            }
            onChange={date => {
              let end_time = Math.round(date._d.getTime() / 1000)
              console.log(end_time)
              this.setState({
                challengeDetail: { ...challengeDetail, end_time }
              })
            }}
          />
          <Form.Check
            checked={this.state.no_end_date}
            className='mb-4'
            onChange={e => {
              console.log(e.currentTarget.checked)
              if (e.currentTarget.checked) {
                let end_time = infinityTime
                this.setState({
                  challengeDetail: { ...challengeDetail, end_time }
                })
              }
              this.setState({ no_end_date: e.currentTarget.checked })
            }}
            label='No end date'
          />

          <div className='d-flex'>
            <Image
              id='photo'
              className='mt-0 mr-2'
              style={{ height: '104px', width: 'auto' }}
              rounded
              src={photo}
            />
            <ImageUpload
              beforeUpload={this.uploadImage}
              loading={this.state.uploading}
            />
          </div>
          <div>
            {this.renderCriteria(criteria)}
            {/* <button
            className='p-0 btn btn-outline-light w-auto pl-2 pr-2 mb-4'
            onClick={this.addCondition}
          >
            Add Condition
          </button> */}
          </div>

          <Form.Group>
            <Form.Check
              checked={is_private === 1 ? true : false}
              onChange={e => {
                if (e.target.checked === 0) {
                  this.setState({
                    challengeDetail: {
                      ...challengeDetail,
                      pwdToJoin: ''
                    }
                  })
                }
                this.setState({
                  challengeDetail: {
                    ...challengeDetail,
                    is_private: e.target.checked ? 1 : 0
                  }
                })
              }}
              label='Private Challenge'
            />
          </Form.Group>
          <Collapse in={is_private === 1 ? true : false}>
            <Form.Group>
              <Form.Label>Password</Form.Label>
              <Form.Control
                type='password'
                placeholder='Password'
                value={pwdToJoin}
                onChange={e => {
                  this.setState({
                    challengeDetail: {
                      ...challengeDetail,
                      pwdToJoin: e.target.value
                    }
                  })
                }}
              />
            </Form.Group>
          </Collapse>
          <div className='d-flex justify-content-end'>
            <Button
              style={{ width: '100px' }}
              className='p-0 mr-2'
              variant='primary'
              type='submit'
            >
              {this.state.type == 'edit' ? 'Update' : 'Create'}
            </Button>
            <Link to='/admin/challenges'>
              <Button
                style={{ width: '100px' }}
                className='p-0'
                variant='danger'
              >
                Cancle
              </Button>
            </Link>
          </div>
        </Form>
      </Spin>
    )
  }
}

export default FormChallenge
