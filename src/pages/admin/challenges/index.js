import React from 'react'
import moment from 'moment'
import { Link, Redirect } from 'react-router-dom'
import 'antd/dist/antd.css'
import jwt from 'jsonwebtoken'
import {
  Spin,
  Modal,
  Button,
  Checkbox,
  DatePicker,
  Tooltip,
  Popconfirm
} from 'antd'
import {
  ComponentStyle,
  ContainStyle,
  OptionTypes,
  ScrollView,
  View,
  TitleStyle
} from './listchallenges.styles'
import {
  wp,
  hp,
  Params,
  Icons,
  METHOD_GET,
  METHOD_DEL
} from '../../../contants/contants'
import Notification from '../../../components/notification'

const { RangePicker } = DatePicker
// import * as api from '../services/api'

class Challenges extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      list: [],
      listDetail: [],
      notitext: '',
      isLoading: true,
      visiable: false,
      sttSlots: false,
      isLoad: false,
      id: '',
      page: 1,
      shouldRedirect: false
    }
  }
  componentDidMount() {
    this.setQueryPage(1)
    let urlParams = this.getUrlParams(window.location.href)
    if (urlParams.hasOwnProperty('page')) {
      this.setState({ page: urlParams.page })
    }
    this.listChallenges(this.state.page)
  }

  getUrlParams = urlOrQueryString => {
    if (urlOrQueryString.indexOf('?') >= 0) {
      const i = urlOrQueryString.indexOf('?')
      const queryString = urlOrQueryString.substring(i + 1)
      if (queryString) {
        return this._mapUrlParams(queryString)
      }
    }

    return {}
  }

  _mapUrlParams = queryString => {
    return queryString
      .split('&')
      .map(function(keyValueString) {
        return keyValueString.split('=')
      })
      .reduce(function(urlParams, [key, value]) {
        if (Number.isInteger(parseInt(value)) && parseInt(value) === value) {
          urlParams[key] = parseInt(value)
        } else {
          urlParams[key] = decodeURI(value)
        }
        return urlParams
      }, {})
  }

  renderItem = (item, index) => {
    const {
      id,
      title,
      venue,
      start_time,
      end_time,
      is_private,
      is_done,
      slots,
      created_time,
      updateTime,
      number_of_participants
    } = item

    let start = moment.unix(start_time).format('DD/MM/YYYY')
    let end = moment.unix(end_time).format('DD/MM/YYYY')
    let create = moment.unix(created_time).format('DD/MM/YYYY')
    let update = moment.unix(updateTime).format('DD/MM/YYYY')

    return (
      <React.Fragment>
        <td className='center'>{index + 1 + 20 * (this.state.page - 1)}</td>
        <td>{title}</td>
        <td>{venue}</td>
        <td className='center'>{is_private === 1 ? 'Yes' : 'No'}</td>
        <td className='center'>{slots}</td>
        <td className='center'>{number_of_participants}</td>
        <td className='center'>{is_done === 1 ? 'Finished' : 'Active'}</td>
        <td className='center'>{start}</td>
        <td className='center'>{end}</td>
        <td className='center'>{create}</td>
        <td className='center'>{update}</td>
        <OptionTypes>
          <Link to={`/admin/challenges/edit/${id}`}>
            <img
              width='18'
              className='mr-2'
              src={Icons.icEdit}
              align='absmiddle'
            />
          </Link>
          <Popconfirm
            title='Are you sure to delete this challenge?'
            onConfirm={() => this.del(id)}
            okText='Yes'
            cancelText='No'
          >
            <img
              style={{ cursor: 'pointer' }}
              width='18'
              src={Icons.icDel}
              align='absmiddle'
            />
          </Popconfirm>
        </OptionTypes>
      </React.Fragment>
    )
  }

  logout = () => localStorage.clear()

  listChallenges = async page => {
    const url = Params.API_URL + `/admin/challenge/list?page=${page}`
    let token = localStorage.getItem('token')
    const data = await fetch(url, {
      method: METHOD_GET,
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
      .then(response => response.json())
      .catch(e => e)
    if (data.status_code === 1000) {
      this.setState({
        list: data.data.items,
        isLoading: false,
        total: data.data.total
      })
    } else {
      this.logout()
      this.setState({ redirectToReferrer: true })
    }
  }

  edit = async id => {
    // console.log("edit",id)
    this.setState({ id })
    this.setState({ visiable: true })
    // let url = Params.API_URL+`/admin/challenge/detail/${id}`
    // let token = localStorage.getItem('token')
    // // console.log(url)
    // const data = await fetch(url,{
    //   method:METHOD_GET,
    //   headers:{
    //     'Content-Type': 'application/json',
    //     'Authorization': 'Bearer ' + token
    //   }
    // })
    // .then(response => response.json())
    // .catch(e=>e)
    // console.log(data)
    // if(data.status_code === 1000){
    //   this.setState({listDetail:data.data})
    // }
  }
  del = async id => {
    const param = {
      challengeId: id
    }
    const url = Params.API_URL + '/admin/challenge/delete'
    let token = localStorage.getItem('token')
    fetch(url, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token
      },
      body: JSON.stringify(param)
    })
      .then(response => response.json())
      .then(data => {
        if (data.status_code === 1000) {
          Notification('success', 'SUCCESS', 'Delete challenge successfully!')
          this.listChallenges(this.state.page)
        } else {
          Notification(
            'error',
            'FAILURE',
            'Delete challenge unsuccessfully. Please try again!'
          )
          this.setState({ isLoading: false })
        }
      })
      .catch(e => e)
  }
  CancelEdit = () => {
    this.setState({ visiable: false, id: '' })
  }
  OkEdit = () => {
    this.setState({ visiable: false, id: '' })
  }

  setQueryPage = page => {
    this.props.history.push({
      pathname: '/admin/challenges',
      search: '?' + new URLSearchParams({ page: page }).toString()
    })
    this.setState({ page })
    this.listChallenges(page)
  }

  renderPagination = total => {
    let pageSize =
      total % 20 === 0 ? parseInt(total / 20) : parseInt(total / 20) + 1
    let children = []
    for (let i = 0; i < pageSize; i++) {
      children[i] = i + 1
    }
    let active = ''
    return (
      <div className='text-right mt-3 mr-3'>
        {children.map(item => {
          if (item == Number(this.state.page)) {
            active = 'text-primary'
          }
          return (
            <a
              className={`pr-2 pl-2 pt-1 pb-1 rounded mr-2 ml-2 ${active}`}
              style={{ border: '1px solid #2b2b2b' }}
              onClick={e => {
                e.preventDefault()
                this.setQueryPage(item)
              }}
            >
              {item}
            </a>
          )
        })}
      </div>
    )
  }

  render() {
    const {
      isLoading,
      list,
      listDetail,
      visiable,
      id,
      isLoad,
      total,
      page
    } = this.state
    console.log('page', page)

    const component = (
      <React.Fragment>
        <table style={{ width: '100%' }} align='center'>
          <thead>
            <tr>
              <th>No.</th>
              <th>Title</th>
              <th>Venue</th>
              <th>Private</th>
              <th>Slots</th>
              <th>
                <Tooltip placement='right' title='Number of Participants'>
                  <span>Participants</span>
                </Tooltip>
              </th>
              <th>Status</th>
              <th>Start Time</th>
              <th>End Time</th>
              <th>Create Time</th>
              <th>Update Time</th>
              <th />
            </tr>
          </thead>
          <tbody>
            {list.map((item, index) => {
              return (
                <tr id={item.id} key={item.id}>
                  {this.renderItem(item, index)}
                </tr>
              )
            })}
          </tbody>
        </table>
      </React.Fragment>
    )

    let token = localStorage.getItem('token')
    var user = jwt.decode(token)
    return user && !this.state.redirectToReferrer ? (
      <ComponentStyle
        style={{ height: '100vh' }}
        className='site_wrapper d-flex  align-items-center justify-content-center '
        id='site_wrapper'
      >
        <Spin spinning={isLoading} delay={500} tip='Loading...'>
          <ContainStyle>
            <ScrollView>
              <Link className='float-left' onClick={this.logout} to='/login'>
                Log out
              </Link>
              <Link className='float-right' to='/admin/challenges/create'>
                New challenge
              </Link>
              <TitleStyle>Challenges</TitleStyle>
              <View>{component}</View>
              {this.renderPagination(total)}
            </ScrollView>
          </ContainStyle>
        </Spin>
      </ComponentStyle>
    ) : (
      <Redirect to={'/login'} />
    )
  }
}

export default Challenges
