import styled from 'styled-components'
import { Backgrounds, wp, hp, Colors } from '../../../contants/contants'

export const CreateChallengeStyle = styled.div`
    background-image: url(${Backgrounds.BgCreateChallenges});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    table{
        width:${wp * 0.34 * 0.83 + 40 + 'px'}
    }
    thead{
        font-weight:bold
        background-color:${Colors.blueTable};
        color:${Colors.white}
    }
    tr:nth-child(even){
        background-color:${Colors.grayTable}
    }
    table, th, td {
        border-left: 1px solid ${Colors.white};
        text-align:center;
        margin-top:15px;
        font-family: 'Lato', sans-serif;
        font-weight:'bold';
        font-size:16px;
      }
    .ant-input{
        width:${
          wp < hp ? wp * 0.934 - 30 + 'px' : wp * 0.34 * 0.83 + 40 + 'px'
        };
    }
    .ant-btn{
        width:${
          wp < hp ? wp * 0.934 - 30 + 'px' : wp * 0.34 * 0.83 + 40 + 'px'
        };
        margin-top:15px
    }
    .slt{
        width:${
          wp < hp ? wp * 0.934 - 30 + 'px' : wp * 0.34 * 0.83 + 40 + 'px'
        };
        margin-bottom:15px
    }

    @media only screen and (max-width:1921px) {
        table{
            width:${wp * 0.34 * 0.83 + 40 + 'px'}
            
        }
        .ant-input{
            width:${
              wp < hp ? wp * 0.934 - 30 + 'px' : wp * 0.34 * 0.83 + 40 + 'px'
            };
        }
        .ant-btn{
            width:${
              wp < hp ? wp * 0.934 - 30 + 'px' : wp * 0.34 * 0.83 + 40 + 'px'
            };
            margin-bottom:15px
        }
        .slt{
            width:${
              wp < hp ? wp * 0.934 - 30 + 'px' : wp * 0.34 * 0.83 + 40 + 'px'
            };
        }
    }
    @media only screen and (max-width:1441px) {
        table{
            width:${wp * 0.34 * 0.83 + 25 + 'px'}
            
        }
        .ant-input{
            width:${wp * 0.34 * 0.83 + 25 + 'px'};
        }
        .ant-btn{
            width:${wp * 0.34 * 0.83 + 25 + 'px'};
            margin-bottom:15px
        }
        .slt{
            width:${wp * 0.34 * 0.83 + 25 + 'px'};
        }
    }

`
export const BgFromCrt = styled.div`
    margin-left: ${
      wp < hp ? (wp - wp * 0.934) / 2 + 'px' : wp / 2 - wp * 0.34 + 'px'
    }
    height: ${wp < hp ? hp * 0.837 + 'px' : hp * 0.837 + 'px'};
    width : ${wp < hp ? wp * 0.934 + 'px' : wp * 0.34 + 'px'};
    background:${Colors.white};
    padding: 30px 0px 30px 35px
    border-radius:8px;
    @media only screen and (max-width:1921px) {
        padding: 30px 0px 30px 35px
    }
    @media only screen and (max-width:1441px) {
        padding: 20px 0px 20px 25px
    }
    @media only screen and (max-width:1281px) {
        padding: 20px 0px 20px 15px
    }
`
export const ScrollView = styled.div`
  height: ${wp < hp ? hp * 0.837 - 20 + 'px' : hp * 0.837 - 60 + 'px'};
  width: ${wp < hp ? wp * 0.934 + 'px' : wp * 0.34 - 45 + 'px'};
  overflow: scroll;
  overflow-x: hidden;
  .ant-input-affix-wrapper {
    width: 96.5% !important;
  }
  @media only screen and (max-width: 1921px) {
    width: ${wp < hp ? wp * 0.934 + 'px' : wp * 0.34 - 45 + 'px'};
    height: ${wp < hp ? hp * 0.837 - 20 + 'px' : hp * 0.837 - 60 + 'px'};
  }
  @media only screen and (max-width: 1441px) {
    width: ${wp < hp ? wp * 0.934 + 'px' : wp * 0.34 - 35 + 'px'};
    height: ${wp < hp ? hp * 0.837 - 20 + 'px' : hp * 0.837 - 40 + 'px'};
  }
  @media only screen and (max-width: 1281px) {
    width: ${wp < hp ? wp * 0.934 + 'px' : wp * 0.34 - 30 + 'px'};
    height: ${wp < hp ? hp * 0.837 - 20 + 'px' : hp * 0.837 - 40 + 'px'};
  }
`
export const InputDescript = styled.textarea`
  resize: none;
  width: ${wp < hp ? wp * 0.934 - 30 + 'px' : wp * 0.34 * 0.83 + 40 + 'px'};
  padding: 5px;
  border: 0;
  margin: 0 0 15px 0;
  background-color: rgb(241, 247, 251);
  border-radius: 8px;
  outline: none;
  height: ${hp * 0.837 * 0.185 + 'px'};

  @media only screen and (max-width: 1921px) {
    width: ${wp < hp ? wp * 0.934 - 30 + 'px' : wp * 0.34 * 0.83 + 40 + 'px'};
  }
  @media only screen and (max-width: 1441px) {
    width: ${wp < hp ? wp * 0.934 - 30 + 'px' : wp * 0.34 * 0.83 + 25 + 'px'};
  }
`
export const InputStyle = styled.input`
  width: ${wp < hp ? wp * 0.934 - 30 + 'px' : wp * 0.34 * 0.83 + 40 + 'px'};
  height: ${hp * 0.837 * 0.045 + 'px'};
  padding: 5px;
  line-height: ${hp * 0.837 * 0.045 + 'px'};
  border: 0;
  margin: 0 0 15px 0;
  background-color: rgb(241, 247, 251);
  border-radius: 8px;
  outline: none;
  :placeholder {
    line-height: ${hp * 0.837 * 0.045 + 'px'};
    font-size: 14px;
  }
  @media only screen and (max-width: 1921px) {
    width: ${wp < hp ? wp * 0.934 - 30 + 'px' : wp * 0.34 * 0.83 + 40 + 'px'};
  }
  @media only screen and (max-width: 1441px) {
    width: ${wp < hp ? wp * 0.934 - 30 + 'px' : wp * 0.34 * 0.83 + 25 + 'px'};
  }
`
export const TitleCrt = styled.h1`
  width: ${wp < hp ? wp * 0.934 - 30 + 'px' : wp * 0.34 * 0.83 + 40 + 'px'}};
  font-family: 'Lato', sans-serif;
  font-weight: 'bold';
  font-size: 28px;
  text-align: center;
  margin: 0 0 15px 0;
`
export const TitleComponent = styled.h3`
  width: ${wp < hp ? wp * 0.934 - 30 + 'px' : wp * 0.34 * 0.83 + 40 + 'px'}};
  font-family: 'Lato', sans-serif;
  font-weight: 'bold';
  font-size: 16px;
  margin-bottom: 15px;
  @media only screen and (max-width: 1921px) {
  }
`
export const BtnAdd = styled.a`
  display: 'inline';
  height: 20px;
  line-height: 20px;
  margin-bottom: 15px;
  img {
    width: 13px;
    margin-top: -4px;
  }
  span {
    font-family: 'Lato', sans-serif;
    font-size: 15px;
    color: #004aff;
  }
`
