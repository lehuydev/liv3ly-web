import styled from "styled-components";
import { Backgrounds ,wp,hp ,Colors} from '../../../contants/contants'

export const ComponentStyle =styled.div`
    background-image: url(${Backgrounds.BgLogin});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    table{
        width: ${wp*0.95-60 +"px"}
        
    }
    table,th,td{
        border-left: 1px solid ${Colors.white};
        font-family: 'Lato', sans-serif;
        font-weight:'bold';
        font-size:16px;
    }
    th{
        padding:10px
        text-align:center
    }
    td{
        padding:3px 8px 3px 8px
        font-size:14px
    }
    .center{
        text-align:center
    }
    td>a{
        margin:auto
        padding:4px 8px
    }
    td > a > img {
        width:18px
        vertical-align:middle
    }
    tr:hover{
        outline: 0.5px solid ${Colors.blueTable}
    }
    tr:nth-child(even){
        background-color:${Colors.grayTable}
    }
    thead{
        font-weight:bold
        background-color:${Colors.blueTable};
        color:${Colors.white}
    }
`
export const ContainStyle = styled.div`
    width: ${wp*0.95 +"px"}
    height: ${hp*0.9 +"px"}
    background-color: ${Colors.white}
    border-radius: 8px
    padding:10px 10px 10px 10px
    `
export const ScrollView = styled.div`
    width: ${wp*0.95-15 +"px"}
    height: ${hp*0.9-20 +"px"}
    overflow:scroll;
    overflow-x: hidden;
`

export const OptionTypes = styled.td`
    display: flex;
    justify-content: space-between;
    align-items:center
`
export const View = styled.div`
    padding:0 20px 0 15px
`
export const TitleStyle = styled.h1`
    font-family: 'Lato', sans-serif;
    font-weight:'bold';
    font-size:30px;
    text-align:center
`
export const TableTdLeft = styled.td`
   
`
export const BtnAdd = styled.a`
    display:"inline";
    height:20px;
    line-height:20px;
    margin-bottom:15px;
    img{
        width:13px;
        margin-top:-4px;

    }
    span{
        font-family: 'Lato', sans-serif;
        font-size:15px;
        color:#004AFF;
    }
`
export const TitleComponent = styled.h3`
    // width:${   wp < hp ? wp*0.934-30 + "px" :  (wp*0.34)*0.83+40 + "px"}};
    font-family: 'Lato', sans-serif;
    font-weight:'bold';
    font-size:16px;
    margin-bottom : 15px;
    @media only screen and (max-width:1921px) {}
`
export const InputDescript = styled.textarea`
    resize:none;
    // width:${  wp < hp ? wp*0.934-30 + "px" :  (wp*0.34)*0.83+40 + "px"};
    padding:5px;
    border:0;
    margin:0 0 15px 0;
    background-color:rgb( 241, 247, 251);
    border-radius: 8px;
    outline:none;
    // height: ${(hp*0.837)*0.185+ "px"};

    @media only screen and (max-width:1921px) {
        // width:${wp < hp ? wp*0.934-30 + "px" :  (wp*0.34)*0.83+40 + "px"};
        
    } 
    @media only screen and (max-width:1441px) {
        // width:${wp < hp ? wp*0.934-30 + "px" :  (wp*0.34)*0.83+25 + "px"};

    } 
`
export const InputStyle = styled.input`
    width:${  wp < hp ? wp*0.934-30 + "px" :  (wp*0.34)*0.83+40 + "px"};
    height: ${(hp*0.837)*0.045+ "px"};
    padding:5px;
    line-height:${(hp*0.837)*0.045+ "px"};
    border:0;
    margin:0 0 15px 0;
    background-color:rgb( 241, 247, 251);
    border-radius: 8px;
    outline:none;
    :placeholder{
        line-height:${(hp*0.837)*0.045+ "px"};
        font-size:14px
    }
    @media only screen and (max-width:1921px) {
        width:${  wp < hp ? wp*0.934-30 + "px" :  (wp*0.34)*0.83+40 + "px"};
    }
    @media only screen and (max-width:1441px) {
        width:${  wp < hp ? wp*0.934-30 + "px" :  (wp*0.34)*0.83+25 + "px"};
    }
`