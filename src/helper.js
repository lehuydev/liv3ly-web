export const getBase64 = file => {
  console.log(file)
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => {
      console.log('get base 64 success')
      resolve(reader.result)
    }
    reader.onerror = error => {
      console.log('get base 64 error', error)
      reject(error)
    }
  })
}

export const getDataUri = (url, callback) => {
  var image = new Image()
  image.crossOrigin = 'anonymous'
  image.onload = function() {
    var canvas = document.createElement('canvas')
    canvas.width = this.naturalWidth // or 'width' if you want a special/scaled size
    canvas.height = this.naturalHeight // or 'height' if you want a special/scaled size
    canvas.getContext('2d').drawImage(this, 0, 0)
    // ... or get as Data URI
    callback(canvas.toDataURL('image/jpeg'))
  }
  image.src = url
}

export const urlToBase64 = url => {
  fetch
    .remote(url)
    .then(data => {
      // data[1] contains base64-encoded jpg with MimeType
      console.log(data)
    })
    .catch(reason => {})
}

export const convertTime = seconds => {
  var hours = Math.floor(seconds / 3600)
  var hourString =
    hours.toString().length === 1 ? `0${hours}` : hours.toString()
  seconds %= 3600
  var minutes = Math.floor(seconds / 60)
  var minuteString =
    minutes.toString().length === 1 ? `0${minutes}` : minutes.toString()
  seconds = seconds % 60
  var secondString =
    seconds.toString().length === 1 ? `0${seconds}` : seconds.toString()
  return hours !== 0
    ? `${hourString}:${minuteString}:${secondString}`
    : `${minuteString}:${secondString}`
}
